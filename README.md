### Dispenser
---
This is an addon for Deathstrider that adds a Dispenser skin for the merchant.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- PillowBlaster, sprites.
- Accensus, coding.
- Wingless Wolpertinger, sprites, brightmaps.

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.